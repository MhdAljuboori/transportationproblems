package Execution;

import TransportationProblems.*;
import TransportationProblems.TransportProblem.Cell;

public class Test {

    public static void main(String[] args) {
        TransportProblem.Cell[][] matrix = new TransportProblem.Cell[3][4];
        matrix[0][0] = new Cell(10.0);
        matrix[0][1] = new Cell(2.0);
        matrix[0][2] = new Cell(20.0);
        matrix[0][3] = new Cell(11.0);
        matrix[1][0] = new Cell(12.0);
        matrix[1][1] = new Cell(7.0);
        matrix[1][2] = new Cell(9.0);
        matrix[1][3] = new Cell(20.0);
        matrix[2][0] = new Cell(4.0);
        matrix[2][1] = new Cell(14.0);
        matrix[2][2] = new Cell(16.0);
        matrix[2][3] = new Cell(18.0);
        double[] p = new double[3];
        p[0] = 15.0;
        p[1] = 25.0;
        p[2] = 10.0;
        double[] d = new double[4];
        d[0] = 5.0;
        d[1] = 15.0;
        d[2] = 15.0;
        d[3] = 15.0;

        int i = 1;
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        NorthWest nw = new NorthWest(matrix.clone(), p.clone(), d.clone());
        while (true) {
            try {
                nw.nextStep();
                System.out.println("Done" + i++);
            } catch (MinimumValues.AlreadyReachedToSolution e) {
                System.out.println(e);
                break;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        TransportProblem t = new TransportProblem(matrix, p, d);
        t.setSolver(new NorthWest());
        t.doFirstSolution();
        Cell[][] c = t.getSolution();
        for (int j = 0; j < c.length; j++) {
            for (int k = 0; k < c[j].length; k++) {
                System.out.print(c[j][k].getValue() + "\t");
            }
            System.out.println("");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        i = 1;
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        MinimumValues minimumValues = new MinimumValues(matrix.clone(), p.clone(), d.clone());
        while (!minimumValues.getReachedToSolution()) {
            try {
                minimumValues.nextStep();
                System.out.println("Done" + i++);
            } catch (MinimumValues.AlreadyReachedToSolution e) {
                System.out.println(e);
                break;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        System.out.println("All Done");
    }
}
