package TransportationProblems;

import TransportationProblems.TransportProblem.*;
import java.util.*;

public class MinimumValues extends TransportProblem.Solver {

    private Set<Cell> basicSet = new HashSet<Cell>();
    private Set<Integer> columnSet = new HashSet<Integer>();

    /**
     * 
     * @param A matrix input Data
     * @param productionAmount vector of Production Amount
     * @param demandAmount vector Demand Amount
     */
    public MinimumValues(Cell[][] A, double[] productionAmount, double[] demandAmount) {
        super(A, productionAmount, demandAmount);
    }

    public MinimumValues() {
        
    }
    

    /**
     * 
     * @return matrix of Cells that have solution
     */
    public Cell[][] getSolution() {
        return A;
    }

    /**
     * Get Minimum value of item in matrix
     * @param matrix the matrix to search the minimum value from it
     * @return the pointer to the minimum value
     */
    public Pointer getMinimum(Cell[][] matrix) {
        Cell min = new Cell(Float.POSITIVE_INFINITY);
        Pointer pointer = new Pointer(-1, -1);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (!columnSet.contains(j)) {
                    if (min.getItem() > matrix[i][j].getItem()) {
                        if (!basicSet.contains(matrix[i][j])) {
                            pointer.setIJ(i, j);
                            min = matrix[i][j];
                        }
                    }
                }
            }
        }
        if (!pointer.isNegative()) {
            basicSet.add(min);
        }
        return pointer;
    }

    /**
     * 
     * @return pointer of last value of to make 
     *          productionAmount and demandAmount vectors values equals to zero
     */
    private Pointer getLast() {
        Pointer p = new Pointer();
        int length = Math.max(productionAmount.length, demandAmount.length);
        int start = 0;
        try {
            for (int i = start; i < length; i++) {
                if (productionAmount[i] != 0) {
                    p.setI(i);
                }
                if (demandAmount[i] != 0) {
                    p.setJ(i);
                }
            }
            start++;
        } catch (ArrayIndexOutOfBoundsException e) {
            if (productionAmount.length == length) {
                for (int j = start; j < length; j++) {
                    if (productionAmount[j] != 0) {
                        p.setI(j);
                    }
                }
            } else if (demandAmount.length == length) {
                for (int j = start; j < length; j++) {
                    if (demandAmount[j] != 0) {
                        p.setJ(j);
                    }
                }
            }
        }
        return p;
    }

    /**
     * Next Step by matrix, productionAmount and demandAmount
     * 
     * @param matrix to get next step
     * @param productionAmount the Production Amount vector
     * @param demandAmount the Demand Amount vector
     */
    private void next(Cell[][] matrix, double[] productionAmount,
            double[] demandAmount) {
        Pointer p = new Pointer();
        if (basicSet.size() < demandAmount.length) {
            p = getMinimum(matrix);
        } else {
            p = getLast();
        }
        while (!p.isNegative() && productionAmount[p.getI()] == 0) {
            p = getMinimum(A);
        }
        if (!p.isNegative()) {
            double minimum = Math.min(productionAmount[p.getI()], demandAmount[p.getJ()]);
            A[p.getI()][p.getJ()].setValue(minimum);
            productionAmount[p.getI()] -= minimum;
            demandAmount[p.getJ()] -= minimum;
            columnSet.add(p.getJ());
        }
    }

    /**
     * Next Step by matrix, productionAmount and demandAmount
     */
    @Override
    public void nextStep() {
        if (reachedToSolution == null) {
            reachedToSolution = reachedSolution();
        }
        if (!reachedToSolution) {
            next(A, productionAmount, demandAmount);
            reachedToSolution = null;
        } else {
            throw new AlreadyReachedToSolution("Next Step");
        }
    }
}
