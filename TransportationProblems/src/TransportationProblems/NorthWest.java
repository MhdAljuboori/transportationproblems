package TransportationProblems;


import TransportationProblems.TransportProblem.*;


public class NorthWest extends TransportProblem.Solver {
    
    
    Pointer p = new Pointer();
    
    /**
    * 
    * @param A Matrix that given
    * @param productionAmount vector
    * @param demandAmount vector
    */
    public NorthWest(Cell[][] A, double[] productionAmount, double[] demandAmount) {
        super(A, productionAmount, demandAmount);
    }

    public NorthWest() {
    }
    
    
    
    /**
     * 
     * @param p to set start item to solve problem
     */
    public void setStartItem(Pointer p) {
        this.p = p;
    }
    
    /**
     * 
     * @return matrix of Cells that have solution
     */
    public Cell[][] getSolution() {
        return A;
    }
    
    /**
     * Next Step by matrix, productionAmount and demandAmount and it's basic Item at Point p
     * 
     * @param matrix to get next step
     * @param productionAmount the Production Amount vector
     * @param demandAmount the Demand Amount vector
     * @param p point of basic item
     */
    private void next(Cell[][] matrix, double[] productionAmount,
            double[] demandAmount, Pointer p) {
        Cell basicItem = matrix[p.getI()][p.getJ()];
        double minimum = Math.min(productionAmount[p.getI()], demandAmount[p.getJ()]);
        basicItem.setValue(minimum);
        productionAmount[p.getI()] -= minimum;
        demandAmount[p.getJ()] -= minimum;
        if (productionAmount[p.getI()] == 0) {
            p.moveDown();
        } else if (demandAmount[p.getJ()] == 0) {
            p.moveRight();
        }
    }

    /**
     * Next Step by matrix, productionAmount and demandAmount
     */
    @Override
    public void nextStep() {
        if (reachedToSolution == null) {
            reachedToSolution = reachedSolution();
        }
        if (!reachedToSolution) {
            next(A, productionAmount, demandAmount, p);
            reachedToSolution = null;
        } else {
            throw new AlreadyReachedToSolution("Next Step");
        }
    }
}
