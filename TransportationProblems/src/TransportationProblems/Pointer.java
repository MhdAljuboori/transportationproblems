package TransportationProblems;


public class Pointer {

    private int i = 0;
    private int j = 0;

    /**
     * create new pointer with i, j equal to zero
     */
    public Pointer() {}
    
    /**
     * create new pointer
     * @param i line value
     * @param j column value
     */
    public Pointer(int i, int j) {
        this.i = i;
        this.j = j;
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }

    /**
     * @return the j
     */
    public int getJ() {
        return j;
    }

    /**
     * @param j the j to set
     */
    public void setJ(int j) {
        this.j = j;
    }
    
    /**
     * 
     * @param i the i to set
     * @param j the j to set
     */
    public void setIJ(int i, int j) {
        this.i = i;
        this.j = j;
    }
    
    /**
     * To move Pointer right
     */
    public void moveRight() {
        j++;
    }
    
    /**
     * To move Pointer left
     */
    public void moveLeft() {
        j--;
    }
    
    /**
     * To move Pointer up
     */
    public void moveUp() {
        i--;
    }
    
    /**
     * To move Pointer down
     */
    public void moveDown() {
        i++;
    }
    
    /**
     * 
     * @return {@code true} if i or j have negative value, {@code false} otherwise
     */
    public boolean isNegative() {
        if (i < 0 || j < 0) {
            return true;
        }
        return false;
    }
}
