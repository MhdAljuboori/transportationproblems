package TransportationProblems;

import java.util.Arrays;

public class TransportProblem {

    Solver solver;
    private Cell[][] A;
    private double[] productionAmount;
    private double[] demandAmount;
    private long time;

    /**
     * @return the A
     */
    public Cell[][] getA() {
        return A;
    }

    /**
     * @param A the A to set
     */
    public void setA(Cell[][] A) {
        this.A = A;
    }

    /**
     * @return the productionAmount
     */
    public double[] getProductionAmount() {
        return productionAmount;
    }

    /**
     * @param productionAmount the productionAmount to set
     */
    public void setProductionAmount(double[] productionAmount) {
        this.productionAmount = productionAmount;
    }

    /**
     * @return the demandAmount
     */
    public double[] getDemandAmount() {
        return demandAmount;
    }

    /**
     * @param demandAmount the demandAmount to set
     */
    public void setDemandAmount(double[] demandAmount) {
        this.demandAmount = demandAmount;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    private void addRow(double diff, Cell[][] A, double[] productionAmount) {
        double[] newP = new double[productionAmount.length + 1];
        for (int i = 0; i < productionAmount.length; i++) {
            newP[i] = productionAmount[i];
        }
        newP[newP.length - 1] = diff;
        this.productionAmount = newP;
        Cell[][] newA = new Cell[newP.length][demandAmount.length];
        for (int i = 0; i < newA.length - 1; i++) {
            for (int j = 0; j < newA[i].length; j++) {
                newA[i][j] = new Cell(A[i][j]);
            }
        }
        for (int i = 0; i < demandAmount.length; i++) {
            newA[newA.length - 1][i] = new Cell();
        }
        this.A = newA;
    }

    private void addColumn(double diff, Cell[][] A, double[] demandAmount) {
        double[] newD = new double[demandAmount.length + 1];
        for (int i = 0; i < demandAmount.length; i++) {
            newD[i] = demandAmount[i];
        }
        newD[newD.length - 1] = diff;
        this.demandAmount = newD;
        Cell[][] newA = new Cell[productionAmount.length][newD.length];
        for (int i = 0; i < newA.length; i++) {
            for (int j = 0; j < newA[i].length - 1; j++) {
                newA[i][j] = new Cell(A[i][j]);
            }
        }
        for (int i = 0; i < productionAmount.length; i++) {
            newA[i][demandAmount.length - 1] = new Cell();
        }
        this.A = newA;
    }

    public static abstract class Solver {

        protected Cell[][] A;
        protected double[] productionAmount;
        protected double[] demandAmount;
        protected Boolean reachedToSolution = null;

        /**
         * @return the A
         */
        public Cell[][] getA() {
            return A;
        }

        /**
         * @param A the A to set
         */
        public void setA(Cell[][] A) {
            this.A = A;
        }

        /**
         * @return the productionAmount
         */
        public double[] getProductionAmount() {
            return productionAmount;
        }

        /**
         * @param productionAmount the productionAmount to set
         */
        public void setProductionAmount(double[] productionAmount) {
            this.productionAmount = productionAmount;
        }

        /**
         * @return the demandAmount
         */
        public double[] getDemandAmount() {
            return demandAmount;
        }

        /**
         * @param demandAmount the demandAmount to set
         */
        public void setDemandAmount(double[] demandAmount) {
            this.demandAmount = demandAmount;
        }

        /**
         * Solves the problem 
         */
        public void solve(Cell[][] A, double[] productionAmount, double[] demandAmount) {
            this.A = A;
            this.productionAmount = productionAmount;
            this.demandAmount = demandAmount;
            while (!getReachedToSolution()) {
                nextStep();
            }
        }

        /**
         * @return {@code flase} if doesn't get the solution, {@code true} otherwise
         */
        protected boolean reachedSolution() {
            int length = Math.max(productionAmount.length, demandAmount.length);
            int start = 0;
            try {
                for (int i = start; i < length; i++) {
                    if (productionAmount[i] != 0) {
                        return false;
                    }
                    if (demandAmount[i] != 0) {
                        return false;
                    }
                }
                start++;
            } catch (ArrayIndexOutOfBoundsException e) {
                if (productionAmount.length == length) {
                    for (int j = start; j < length; j++) {
                        if (productionAmount[j] != 0) {
                            return false;
                        }
                    }
                } else if (demandAmount.length == length) {
                    for (int j = start; j < length; j++) {
                        if (demandAmount[j] != 0) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /**
         * 
         * @return matrix of Cells that have solution
         */
        public abstract Cell[][] getSolution();

        /**
         * 
         * @return value of Z
         */
        public double getZ() {
            double Z = 0.0;
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A[i].length; j++) {
                    if (A[i][j].getValue() != 0) {
                        Z += A[i][j].getItem() * A[i][j].getValue();
                    }
                }
            }
            return Z;
        }

        /**
         * Next Step by matrix, productionAmount and demandAmount
         */
        public abstract void nextStep();

        /**
         * @return the reachedToSolution
         */
        public Boolean getReachedToSolution() {
            return reachedToSolution = reachedSolution();
        }

        /**
         * 
         * @param A Matrix that given
         * @param productionAmount vector
         * @param demandAmount vector
         */
        public Solver(Cell[][] A, double[] productionAmount, double[] demandAmount) {
            this.A = A;
            this.productionAmount = productionAmount;
            this.demandAmount = demandAmount;
        }

        public Solver() {
            // does nothing
        }

        public static class AlreadyReachedToSolution extends RuntimeException {

            String operationName;

            public AlreadyReachedToSolution(String operationName) {
                this.operationName = operationName;
            }

            public AlreadyReachedToSolution() {
                operationName = "";
            }

            @Override
            public String toString() {
                return String.format("Operation %s Already Reached To Solution", operationName);
            }
        }
    }

    public static class Cell implements Cloneable {

        private double item = 0.0;
        private double value = 0.0;

        public Cell() {
        }

        /**
         * 
         * @param item value of Item
         */
        public Cell(double item) {
            this.item = item;
        }

        /**
         * 
         * @param item value of Item
         * @param value not zero if item is basic, zero otherwise
         */
        public Cell(double item, double value) {
            this.item = item;
            this.value = value;
        }

        /**
         * 
         * @param cell to copy it to this object
         */
        public Cell(Cell cell) {
            this.item = cell.item;
            this.value = cell.value;
        }

        /**
         * @return the item value
         */
        public double getItem() {
            return item;
        }

        /**
         * @param item the item value to set
         */
        public void setItem(double item) {
            this.item = item;
        }

        /**
         * @return the value
         */
        public double getValue() {
            return value;
        }

        /**
         * @param value the value to set
         */
        public void setValue(double value) {
            this.value = value;
        }

        @Override
        public Object clone() {
            Cell cell = new Cell(this.item, this.value);
            return cell;
        }

        @Override
        public String toString() {
            //return String.format("%.2f(%.2f)",getValue(),getItem());
            return String.valueOf(getValue());
        }
    }

    public TransportProblem(Cell[][] A, double[] productionAmount, double[] demandAmount) {
        this.A = A;
        this.productionAmount = productionAmount;
        this.demandAmount = demandAmount;
        double diff = getTotalProduction() - getTotalDemand();
        if (diff < 0) {
            addRow(-diff, A, productionAmount);
        } else if (diff > 0) {
            addColumn(diff, A, demandAmount);
        }
    }

    public TransportProblem() {
    }

    public double getTotalProduction() {
        double acc = 0;
        for (double a : productionAmount) {
            acc += a;
        }
        return acc;
    }

    public double getTotalDemand() {
        double acc = 0;
        for (double a : demandAmount) {
            acc += a;
        }
        return acc;
    }

    /**
     * 
     * @param solver type of solver to solve the problem
     */
    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    /**
     * Solve the problem to get first solution
     */
    public void doFirstSolution() {
        if (solver != null) {
            long t1 = System.nanoTime();
            solver.solve(getA(), productionAmount, demandAmount);
            long t2 = System.nanoTime();
            time = t2 - t1;
        } else {
            throw new NullPointerException("Solver not set");
        }
    }

    public void initiate() {
        doFirstSolution();
    }

    /**
     * 
     * @return matrix after solved it
     */
    public Cell[][] getSolution() {
        return solver.getSolution();
    }
    
    public double getZ() {
        return this.solver.getZ();
    }
}
